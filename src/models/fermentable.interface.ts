export interface Fermentable {
  category: string,
  categoryDisplay: string,
  createDate: string,
  id: number,
  name: string,
  description: string,
  country: {
    createDate: string,
    displayName: string,
    isoCode: string,
    isoThree: string,
    name: string,
    numberCode: number
  },
  countryOfOrigin: string,
  dryYield: number,
  maxInBatch: number,
  moistureContent: number,
  potential: number,
  protein: number,
  requiresMashing: string,
  srm: {
    hex: string,
    id: number,
    name: string
  },
  srmId: number,
  srmPrecise: number,
  updateDate: Date
}
