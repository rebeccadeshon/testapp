export interface Brewery {
  createDate: Date,
  updateDate: Date,
  established: Date,
  description: string,
  id: string,
  isOrganic: string,
  isMassOwned: string,
  name: string,
  shortNameDisplay: string,
  status: string,
  statusDisplay: string,
  website: string,
  postCount: number,
  followersCount: number,
  images: {
    icon: string,
    large: string,
    medium: string,
    squareLarge: string,
    squareMedium: string
  }
}
