export interface User {
    bio: string,
    dateCreated: Date,
    email: string,
    followersCount: number,
    followingCount: number,
    profileImage: Blob,
    isBrewer: boolean,
    name: string,
    postCount: number,
    provider: string,
}
