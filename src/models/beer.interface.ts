import { Style } from './models';

export interface Beer {
  id: string,
  abv: string,
  createDate: Date,
  description: string,
  ibu: string,
  isOrganic: string,
  name: string,
  nameDisplay: string,
  postCount: number,
  status: string,
  statusDisplay: string,
  styleId: number,
  style: Style,
  updateDate: Date
}
