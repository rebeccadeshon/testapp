import { Category } from './models';

export interface Style {
  id: number,
  srmMax: number,
  ibuMax: number,
  srmMin : number,
  description : string,
  fgMin : number,
  ibuMin : number,
  createDate : Date,
  fgMax : number,
  abvMax : number,
  ogMin : number,
  abvMin : number,
  name : string,
  categoryId : number,
  category: Category
}
