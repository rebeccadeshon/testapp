export interface Club {
  id: string,
  name: string;
  city: string;
  state: string;
  email: string;
  phone: string;
  url: string;
}
