export interface Event {
  createDate: Date,
  established: Date,
  updateDate: Date,
  startDate: Date,
  endDate: Date,
  year: Date,
  venueName: string,
  locality: string,
  description: string,
  id: string,
  isOrganic: string,
  isMassOwned: string,
  name: string,
  shortNameDisplay: string,
  status: string,
  statusDisplay: string,
  website: string,
  countryIsoCode: string,
  type: string,
  typeDisplay: string,
  region: string,
  streetAddress: string,
  postalCode: string,
  price: string,
  time: string,
  postCount: number,
  followersCount: number,
  latitude: number,
  longitude: number,
  images: {
    icon: string,
    large: string,
    medium: string
  },
  country: {
    createDate: string,
    displayName: string,
    isoCode: string,
    isoThree: string,
    name: string,
    numberCode: number
  }
}
