export interface Ingredient {
  category: string,
  categoryDisplay: string,
  createDate: string,
  id: number,
  name: string,
}
