import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Beer, Brewery, Category, Club, Event, Fermentable, Hops, Ingredient, Style } from '../models/models';

/*
  Generated class for the MongoProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class DataProvider {

  private baseUrl: string = "http://localhost:8080";

  constructor(public http: HttpClient) {
    console.log('Hello DataProvider');
  }

  /******************************/
  /***********BEER API***********/
  /******************************/
  public getById(collection: string, id: string): Observable<any> {
      console.log(`getting ${id} from ${collection}`);
      return this.http.get(`${this.baseUrl}/${collection}?id=${id}`);
  }

  public getByName(collection: string, name: string): Observable<any> {
      console.log(`getting ${name} from ${collection}`);
      return this.http.get(`${this.baseUrl}/${collection}?name=${name}`);
  }

  public getRecent(collection: string): Observable<any> {
      console.log(`getting recent ${collection}`);
      return this.http.get(`${this.baseUrl}/${collection}?recent=true`);
  }

  public getTrending(collection: string): Observable<any> {
      console.log(`getting trending ${collection}`);
      return this.http.get(`${this.baseUrl}/${collection}?trending=true`);
  }

  public getAll(collection: string): Observable<any> {
    console.log(`getting all ${collection}`);
    return this.http.get(`${this.baseUrl}/${collection}`);
  }

  public postBeer(): any {}

  public deleteBeer(): any {}

  public updateBeers(): any {}

  /*BREWER API*/
  public getBrewer(): any {}

  public getBrewers(): any {}

  public getAllBrewers(): any {}

  public postBrewer(): any {}

  public deleteBrewer(): any {}

  public updateBrewer(): any {}

  /*BREWERY API*/
  public getBrewery(): any {}

  public getBreweries(): any {}

  public getAllBreweries(): any {}

  public postBreweries(): any {}

  public deleteBreweries(): any {}

  public updateBreweries(): any {}

  /*CLUB API*/
  public getClub(): any {}

  public getClubs(): any {}

  public getAllClubs(): any {}

  public postClub(): any {}

  public deleteClub(): any {}

  public updateClub(): any {}

  /*EVENT API*/
  public getEvent(): any {}

  public getEvents(): any {}

  public getAllEvents(): any {}

  public postEvent(): any {}

  public deleteEvent(): any {}

  public updateEvent(): any {}

  /*POST API*/
  public getPost(): any {}

  public getPosts(): any {}

  public getAllPosts(): any {}

  public postPost(): any {}

  public deletePost(): any {}

  public updatePost(): any {}

  /*USER API*/
  public getUser(): any {}

  public getUsers(): any {}

  public getAllUsers(): any {}

  public postUser(): any {}

  public deleteUser(): any {}

  public updateUser(): any {}


}
