import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { DataProvider } from '../providers/data';

@Injectable()
export class SearchProvider {
  constructor(public dataProvider: DataProvider) {}

  //return a collection of filtered data using a given data entry type
  //and a given search key
  filter(dataType: string, attribute: string, searchParam: string, brewer?: boolean): Observable<any> {
      if (attribute === "name") {
          if (searchParam === "") {
              return this.dataProvider.getAll(dataType);
          } else {
              return this.dataProvider.getByName(dataType, searchParam);
          }
      } else {
          return null;
      }
  }


}
