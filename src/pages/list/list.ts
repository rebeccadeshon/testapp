import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SearchProvider, DataProvider } from '../../providers/providers';
import { Beer, Brewery, Brewer } from '../../models/models';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
    private searchBeers: Array<Brewer> = [];
    private filter: string;
    private keypress: number = 0;

    constructor(public navCtrl: NavController, public navParams: NavParams,
        public searchProvider: SearchProvider, public dataProvider: DataProvider) { }

    ionViewDidLoad() {
        this.dataProvider.getAll("brewers")
            .subscribe(beers => {
                for (var key in beers) {
                    this.searchBeers.push(beers[key]);
                }
            });
    }

    search($event) {
        if (($event.timeStamp - this.keypress) > 200) {
            /*
              all beer names have quotations
              adding quotes at the start of the string
              allows for use of the high unicode at the end
            */
            this.searchBeers = [];
            this.searchProvider.filter("brewers", "name", this.filter)
                .subscribe(searchResponse => {
                    for (var key in searchResponse) {
                        this.searchBeers.push(searchResponse[key]);
                    }
                });
        }
        this.keypress = $event.timeStamp;
    }

    onBeerClick(beer) {
        console.log(beer.name);
    }

    
}
