import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { DataProvider } from '../../providers/data';
import { Beer, Brewery } from '../../models/models';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  private beers: Array<Beer> = [];

  constructor(public navCtrl: NavController, public dataProvider: DataProvider) {}

  ionViewDidLoad() {
      console.log("hello world");
      this.dataProvider.getAll("beers").subscribe((beerData) => {
          for (var key in beerData) {
              console.log(key, beerData[key]);
              this.beers.push(beerData[key]);
          }
      });      
  }

}
