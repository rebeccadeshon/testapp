import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrewersPage } from './brewers';

@NgModule({
  declarations: [
    BrewersPage,
  ],
  imports: [
    IonicPageModule.forChild(BrewersPage),
  ],
})
export class BrewersPageModule {}
